import axios from 'axios'

function get (url) {
  const requestOptions = {
    method: 'GET'
  }
  return fetch('/api/' + url, requestOptions).then(handleResponse)
}

function extGet (url) {
  const requestOptions = {
    method: 'GET'
  }
  return fetch(url, requestOptions)
}

function post (url, body, options) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body)
  }
  if (options) {
    requestOptions.headers = options.headers
    requestOptions.responseType = options.responseType
  }
  // console.log('requestoptions', body)
  return fetch('/api/' + url, requestOptions).then(handleResponse)
}

function put (url, body) {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(body)
  }
  return fetch('/api/' + url, requestOptions).then(handleResponse)
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete (url) {
  const requestOptions = {
    method: 'DELETE'
  }
  return fetch('/api/' + url, requestOptions).then(handleResponse)
}

function handleResponse (response) {
  if (response.status === 401) throw new Error('Richiesta non autorizzata.')

  return response.text().then(text => {
    const data = text && JSON.parse(text)

    if (!response.ok) {
      const error = (data && data.message) || response.statusText
      return Promise.reject(error)
    }

    return data
  })
}

const fetchWrapper = {
  get,
  post,
  put,
  delete: _delete,
  extGet
}

export default ({ app }) => {
  app.config.globalProperties.$api = fetchWrapper
  app.config.globalProperties.$http = axios
}

const api = fetchWrapper

export {api}
