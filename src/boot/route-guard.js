export default ({ app, router }) => {
  router.beforeEach((to, from, next) => {
    if (['home', 'registrazione', 'cambiopassword', 'modificapassword'].indexOf(to.name) > -1) {
      next()
    } else if (['ordiniCrui'].indexOf(to.name) > -1) {
      app.config.globalProperties.$api.get('auth/is-crui')
        .then(data => next())
        // eslint-disable-next-line
        .catch(error => {
          app.config.globalProperties.$api.get('auth/is-authenticated')
            .then(data => next({ name: 'enti' }))
            // eslint-disable-next-line
            .catch(error => next({ name: 'home' }))
        })
    } else if (['enti', 'ente', 'ftecalcolati', 'ordini', 'requests', 'users'].indexOf(to.name) > -1) {
      app.config.globalProperties.$api.get('auth/is-admin')
        .then(data => next())
        // eslint-disable-next-line
        .catch(error => {
          app.config.globalProperties.$api.get('auth/is-authenticated')
            .then(data => next({ name: 'dashboard' }))
            // eslint-disable-next-line
            .catch(error => next({ name: 'home' }))
        })
    } else {
      app.config.globalProperties.$api.get('auth/is-authenticated')
        .then(data => next())
        // eslint-disable-next-line
        .catch(error => next({ name: 'home' }))
    }
  })
}
