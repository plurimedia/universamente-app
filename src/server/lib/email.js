let mandrill = require('mandrill-api/mandrill'),
    mandrill_client = new mandrill.Mandrill(process.env.MANDRILL_API_KEY),
    juice = require('juice'), // mette gli stili in linea in un html
    fs = require('fs'),
    Mustache = require('mustache'),
    template_css = fs.readFileSync(process.cwd() + '/src/server/templates/email/email.css', 'utf8'), // css base (usiamo zurb per email: http://foundation.zurb.com/emails)
    template_header = fs.readFileSync(process.cwd() + '/src/server/templates/email/header.html', 'utf8'), // header comune a tutte le mail
    template_footer = fs.readFileSync(process.cwd() + '/src/server/templates/email/footer.html', 'utf8'), // footer comune a tutte le mail
    header = Mustache.render(template_header, { css: template_css }),
    message;

// mando!
const mando = function (res) {
  mandrill_client.messages.send({
    message: message,
    async: false // singola mail non serve un batch
  },
  function (result) {
    if (res && res.status)
      res.status(200).send(result)
    else {
      console.log(result)
      res.status(500).send(result)
    }
  })
}

exports.richiestaInfo = function (host, res) {
  let tpl = fs.readFileSync(process.cwd() + '/src/server/templates/email/richiesta_info.html', 'utf8')
  message = { // preparo il messaggio
    html: juice(Mustache.render(tpl, {
      messaggio: 'Hai una nuova richiesta informazioni sulla piattaforma. Accedi per vederla:',
      host: host,
      header: header, // comune
      footer: template_footer // comune
    })),
    subject: 'Nuova richiesta informazioni',
    from_email: 'tech@plurimedia.it',
    from_name: 'Mailer UniversaMente',
    to: [{ 'email': 'maurizio.galotti@plurimedia.it' }]
  }

  mando(res)
}

exports.offertaInserita = function (host, res) {
  let tpl = fs.readFileSync(process.cwd() + '/src/server/templates/email/richiesta_info.html', 'utf8')
  message = { // preparo il messaggio
    html: juice(Mustache.render(tpl, {
      messaggio: 'C\'è una nuova richiesta di offerta sulla piattaforma. Accedi per vederla:',
      host: host,
      header: header, // comune
      footer: template_footer // comune
    })),
    subject: 'Nuova richiesta di offerta',
    from_email: 'tech@plurimedia.it',
    from_name: 'Mailer UniversaMente',
    to: [{ 'email': 'maurizio.galotti@plurimedia.it' }]
  }

  mando(res)
}

exports.nuovoUtente = function (host, res) {
  let tpl = fs.readFileSync(process.cwd() + '/src/server/templates/email/richiesta_info.html', 'utf8')
  message = { // preparo il messaggio
    html: juice(Mustache.render(tpl, {
      messaggio: 'Un nuovo utente si è registrato alla piattaforma. Accedi per abilitarlo:',
      host: host,
      header: header, // comune
      footer: template_footer // comune
    })),
    subject: 'Nuovo utente registrato',
    from_email: 'tech@plurimedia.it',
    from_name: 'Mailer UniversaMente',
    to: [{ 'email': 'maurizio.galotti@plurimedia.it' }]
  }

  if (res) mando(res)
}

exports.utenteAbilitato = function (mail, host, res) {
  let tpl = fs.readFileSync(process.cwd() + '/src/server/templates/email/richiesta_info.html', 'utf8')
  message = { // preparo il messaggio
    html: juice(Mustache.render(tpl, {
      messaggio: 'Sei stato abilitato all\'accesso. Accedi con il seguente link:',
      host: host,
      header: header, // comune
      footer: template_footer // comune
    })),
    subject: 'Attivazione account UniversaMente',
    from_email: 'tech@plurimedia.it',
    from_name: 'Mailer UniversaMente',
    to: [{ 'email': mail }]
  }

  mando(res)
}

exports.cambioStatoOrdineUtente = function (mail, host, res) {
  let tpl = fs.readFileSync(process.cwd() + '/src/server/templates/email/cambio_statoUtente.html', 'utf8')
  message = { // preparo il messaggio
    html: juice(Mustache.render(tpl, {
      messaggio: 'Hai un documento in attesa di firma. Accedi con il seguente link:',
      host: host,
      header: header, // comune
      footer: template_footer // comune
    })),
    subject: 'Offerta in attesa di firma UniversaMente',
    from_email: 'tech@plurimedia.it',
    from_name: 'Mailer UniversaMente',
    to: [{ 'email': mail }]
  }

  mando(res)
}

exports.cambioStatoOrdineCrui = function (host, res) {
  let tpl = fs.readFileSync(process.cwd() + '/src/server/templates/email/cambio_statoCrui.html', 'utf8')
  message = { // preparo il messaggio
    html: juice(Mustache.render(tpl, {
      messaggio: 'Hai un documento in attesa di approvazione. Accedi con il seguente link:',
      host: host,
      header: header, // comune
      footer: template_footer // comune
    })),
    subject: 'Ordine in attesa di approvazione UniversaMente',
    from_email: 'tech@plurimedia.it',
    from_name: 'Mailer UniversaMente',
    to: [{ 'email': 'maurizio.galotti@plurimedia.it' }, { 'email': 'gilesi@crui.it' }]
  }

  mando(res)
}

exports.cambioPassword = function (token, mail, host, res) {
  let tpl = fs.readFileSync(process.cwd() + '/src/server/templates/email/cambio_password.html', 'utf8')
  message = {
    html: juice(Mustache.render(tpl, {
      token: token,
      host: host,
      header: header,
      footer: template_footer
    })),
    subject: 'UniversaMente - Richiesta cambio password',
    from_email: 'tech@plurimedia.it',
    from_name: 'Mailer UniversaMente',
    to: [{ 'email': mail }]
  }

  mando(res)
}
