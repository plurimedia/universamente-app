/* eslint-disable no-useless-escape */
const sql = require('../db')
const email = require('../lib/email')

// Retrieve a particular item from the bucket
exports.getItem = async (req, res) => {
  const itemName = req.query.filename
  const signed = await signedUrlDownload(itemName)
  res.status(200).send({ url: signed })
}

exports.deleteItem = async (req, res) => {
  let itemName = req.query.filename
  const response = await deleteFile(itemName)
  res.status(200).send(response)
}

// Multi part upload
exports.upload = async (req, res) => {
  // restituisco solo la signed url, l'upload lo fa q-uploader sul client
  const newName = Date.now() + req.body.percorso
  const signed = await signedUrl(newName, req.body.tipo)
  res.status(200).send({ url: signed, name: newName })
}

exports.postUpload = async (req, res) => {
  const doc = {
    ordine_id: +req.params.id,
    utente_id: req.user.utente_id,
    nome: req.body.newName,
    cos_id: req.body.newName
  }

  const upl = await sql`insert into ordini_documenti ${sql(doc)} returning *`
  const host = `${req.protocol}://${req.headers.host}`
  if (req.user.admin === 1) {
    const [cambiaStato] = await sql`update ordini set stato = 'E' where ordine_id = ${parseInt(req.params.id)} and stato is null returning utente_id`
    const [mail] = await sql`select mail from utenti where utente_id = ${cambiaStato.utente_id}`
    email.cambioStatoOrdineUtente(mail.mail, host, res)
  } else if (req.user.admin === 0) {
    const cambiaStato = await sql`update ordini set stato = 'F' where ordine_id = ${req.params.id} and stato = 'E'`
    email.cambioStatoOrdineCrui(host, res)
  }
}

let signedUrlDownload, signedUrl, deleteFile
const loadES6Module = async () => {
  const upload = await import('../lib/upload.mjs')
  signedUrlDownload = upload.signedUrlDownload
  signedUrl = upload.signedUrl
  deleteFile = upload.deleteFile
}

loadES6Module()
