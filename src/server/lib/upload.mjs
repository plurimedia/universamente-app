import { PutObjectCommand, GetObjectCommand, HeadObjectCommand, DeleteObjectCommand } from '@aws-sdk/client-s3'
import { getSignedUrl } from '@aws-sdk/s3-request-presigner'
import { S3Client } from '@aws-sdk/client-s3'

const awsBucket = process.env.AWS_BUCKET
const awsRegion = process.env.AWS_REGION
const awsAccessKey = process.env.AWS_ACCESS_KEY_ID
const awsSecretAccessKey = process.env.AWS_SECRET_ACCESS_KEY
const s3 = new S3Client({
  credentials: {
    accessKeyId: awsAccessKey,
    secretAccessKey: awsSecretAccessKey
  }
})

export const signedUrl = async (path, tipo) => {
  const s3Params = {
    Bucket: awsBucket,
    Key: path,
    ContentType: tipo
  }

  const command = new PutObjectCommand(s3Params)
  const opts = { expiresIn: 60 }
  const signed = await getSignedUrl(s3, command, opts)
  return signed
}

export const signedUrlDownload = async (path) => {
  const s3Params = {
    Bucket: awsBucket,
    Key: path
  }

  const command = new GetObjectCommand(s3Params)

  const signed = await getSignedUrl(s3, command, { expiresIn: 60 })
  return signed
}

export const deleteFile = async (path) => {
  const s3Params = {
    Bucket: awsBucket,
    Key: path
  }

  const command = new DeleteObjectCommand(s3Params)
  const response = await s3.send(command)
  return response
}

// https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/client/s3/command/HeadObjectCommand/
export const headFile = async (path, hash) => {
  const s3Params = {
    Bucket: awsBucket,
    Key: path
  }

  const command = new HeadObjectCommand(s3Params)
  const response = await s3.send(command)
  return response  
}
