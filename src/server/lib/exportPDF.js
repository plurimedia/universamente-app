const fs = require('fs')
const pdf = require('html-pdf-node')
const mustache = require('mustache')
const ut = require('./utils')
const template = fs.readFileSync(
  process.cwd() + '/src/server/templates/pdf/ordinePDF.html',
  'utf8'
)

exports.exportOrdine = (req, res) => {
  res.setHeader('Content-Type', 'application/octet-stream')

  ut.prepareData(req.body)

  let myhtml = mustache.render(template, req.body)
  let options = { format: 'A4' }
  let file = { content: myhtml }
  pdf.generatePdf(file, options)
    .then((pdfBuffer) => {
      res.send({ data: pdfBuffer })
    })
    .catch((err) => {
      console.log('Errore in exportPDF pdf inside', err)
      res.status(500).send(err)
    })
}
