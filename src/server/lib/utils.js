exports.prepareData = (ordine) => {
  var newDate = Date.now()
  var date = new Date(newDate);
  var data_formattata =  ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear()

  ordine.data_formattata = data_formattata

  var codice_data = ("0" + date.getDate()).slice(-2) + ("0" + (date.getMonth() + 1)).slice(-2) + date.getFullYear()
  ordine.codice_techData = 'OA' + leadingZeroes(ordine.ordine_id, 4) + codice_data + 'CR'

  function leadingZeroes(number, length) {
    var myString = '' + number
    while (myString.length < length) myString = '0' + myString
    return myString
  }

  let prezzo_scontato = 0
  let totale = [], val_unitario, prezzo_tot, unitario_scontato, totale_ordine, totale_tu = 0

  ordine.prodotti.forEach(function (arrayItem) {
    if (arrayItem.codice === 'MCT0032US') totale_tu += arrayItem.qta
    else if (arrayItem.classe === 'label-corsi') totale_tu += (arrayItem.tu * arrayItem.qta)
  })
  ordine.prodotti.forEach(function (arrayItem) {
    if (arrayItem.codice === 'MCT0032US' || arrayItem.classe === 'label-corsi') {
      if (totale_tu < 71) arrayItem.val_unitario = 180
      else if (totale_tu < 161) arrayItem.val_unitario = 162
      else if (totale_tu < 281) arrayItem.val_unitario = 153
      else if (totale_tu < 411) arrayItem.val_unitario = 144
      else if (totale_tu < 551) arrayItem.val_unitario = 140.4
      else arrayItem.val_unitario = 135

      if (arrayItem.classe === 'label-corsi')
        arrayItem.val_unitario = arrayItem.val_unitario * arrayItem.tu
    }

    if (arrayItem.classe === 'label-consulting') {
      arrayItem.periodo = 'Giornata'
      val_unitario = arrayItem.val_unitario
    } else if (arrayItem.periodo === '3') {
      arrayItem.periodo = '3 anni'
      val_unitario = arrayItem.val_unitario_3
    } else {
      arrayItem.periodo = '1 anno'
      val_unitario = arrayItem.val_unitario
    }
    prezzo_tot = arrayItem.qta * val_unitario

    arrayItem.prezzo_non_scontato = new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(prezzo_tot)
    arrayItem.val_unitario = new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(val_unitario)
    unitario_scontato = val_unitario - (val_unitario * arrayItem.sconto / 100)
    prezzo_scontato = prezzo_tot - (prezzo_tot * arrayItem.sconto / 100)

    arrayItem.sconto += '%'
    arrayItem.unitario_scontato = new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(unitario_scontato)
    arrayItem.prezzo = new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(prezzo_scontato)
    totale.push(prezzo_scontato)
    totale_ordine = totale.reduce((a, b) => a + b, 0)
    ordine.totale_ordine = new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR' }).format(totale_ordine)
    ordine.totale_tu = totale_tu
  })
}
