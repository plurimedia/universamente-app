const jwt = require('jsonwebtoken')
const crypto = require('crypto')
const sql = require('../db.js')
const email = require('./email.js')

exports.getUserInfo = async function (id, done) {
  const [user] = await sql`
    select utente_id, ente_id, admin, nome, mail, fl_ordini
      from utenti
     where utente_id = ${id}
  `

  if (user) return done(null, user)

  return done('ID errato')
}

exports.authenticateLocal = async function (username, password, done) {
  if (username && password) {
    const hash = crypto.createHash('sha512')
    hash.update(password)
    password = hash.digest('hex') // converto password in sha512-hex per confrontarla con quella sul db

    const [user] = await sql`
      select utente_id, ente_id, admin, fl_ordini
        from utenti
       where mail      = ${username}
         and password  = ${password}
         and abilitato = true
         and (admin > 0 or ente_id is not null)
    `

    if (user) return done(null, user)

    return done('Credenziali errate')
  } else {
    setTimeout(function () {
      return done('Credenziali mancanti')
    }, 1000)
  }
}

exports.resetPassword = async function(username, host, res)
{
  if (username)
  {
    const [user] = await sql`
      select utente_id
        from utenti
       where mail      = ${username}
         and abilitato = true
         and (admin > 0 or ente_id is not null)
    `

    if (user) {
      let token = jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: '24h' })
      email.cambioPassword(token, username, host, res)
    }
    else
      res.status(401).send({ esito: 'Email errata' })
  }
}

exports.validateToken = async function(token)
{
   jwt.verify(token, process.env.TOKEN_SECRET)
}

exports.changePassword = async function (body) {
  if (body.password && body.token) {
    const decoded = jwt.verify(body.token, process.env.TOKEN_SECRET)
    const hash = crypto.createHash('sha512')
    hash.update(body.password)
    body.password = hash.digest('hex') // converto password in sha512-hex per salvarla sul db
    await sql`
      update utenti
         set password = ${body.password}
       where utente_id = ${decoded.utente_id}
    `
  } else { throw new Error('Dati errati') }
}
