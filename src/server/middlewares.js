module.exports.isAuth = (req, res, next) => {
  // if (req.isAuthenticated() && (req.user.admin>0 || req.user.ente_id)) next()
  if (req.isAuthenticated()) next()
  else res.status(401).json({ msg: 'Questa risorsa è riservata agli utenti autorizzati' })
}

module.exports.isAdmin = (req, res, next) => {
  if (req.isAuthenticated() && req.user.admin===1) next()
  else res.status(401).json({ msg: 'Questa risorsa è riservata agli utenti Amministratori' })
}

module.exports.isCrui = (req, res, next) => {
  if (req.isAuthenticated() && req.user.admin===2 || req.isAuthenticated() && req.user.admin===3) next()
  else res.status(401).json({ msg: 'Questa risorsa è riservata agli utenti Crui' })
}
