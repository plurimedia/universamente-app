const cors = require('cors')
const path = require('path')
const express = require('express')
const session = require('express-session')
const compression = require('compression')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const {promisify} = require('util')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const auth = require('./lib/auth')
const api = require('./api') // rotte api

let initCallback

global.__basedir = __dirname

// configure passport.js to use the local strategy
passport.use('local', new LocalStrategy({ usernameField: 'username', passwordField: 'password' }, auth.authenticateLocal))
// tell passport how to serialize the user
passport.serializeUser((user, done) => {
  done(null, user.utente_id)
})
passport.deserializeUser(auth.getUserInfo)

const app = express()

console.info('🚀 SERVER started: env %s', process.env.NODE_ENV)

app.use(cors())

const sess = {
  store: new (require('connect-pg-simple')(session))(),
  secret: process.env.TOKEN_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 6000000 }
}
if (process.env.NODE_ENV !== 'development') {
  app.set('trust proxy', 1) // trust first proxy
  sess.cookie.secure = true // serve secure cookies
}
app.use(session(sess))
app.use(passport.initialize())
app.use(passport.session())

app.use((req, res, next) => {
  if (req.headers['x-forwarded-proto'] !== 'https' && process.env.NODE_ENV !== 'development') {
    var redirect = `https://${req.header('host')}${req.url}`
    console.log('Reindirizzo alla home in versione https %s', redirect)
    res.redirect(redirect)
  } else {
    next()
  }
})

app.use(bodyParser.json({ limit: '5mb' })) // parsa la richieste e interpretale come json
app.use(compression())
app.use(helmet({ contentSecurityPolicy: false }))

app.use(express.static(path.join(process.cwd(), 'dist/spa'))) // servi la index del client
app.use('/api', api)

// pushstate per l'app Quasar: servi la index con un 200 su tutti i not found quando non sono in dev
if (process.env.NODE_ENV !== 'development') {
  app.get('*', (req, res, next) => {
    res.status(200).sendFile(path.join(process.cwd(), '/dist/spa/index.html'))
  })
}

var server = app.listen(process.env.PORT || 3000, function () {
  if (initCallback) initCallback()
})
server.timeout = 1000000

async function stopHTTPServer() {
  console.log('Closing HTTP Server')
  await promisify(server.close.bind(server))()
  console.log('HTTP Server stopped')
}
async function closeDBConnection() {
  const sql = require('./db')
  console.log('Closing DB Connection')
  await sql.end({ timeout: 5 })
  console.log('DB Connection closed')
}

process.on('SIGTERM', async () => {
  console.info('SIGTERM signal received')
  await stopHTTPServer()
  await closeDBConnection()
  process.exit(0)
})
process.on('SIGINT', async () => {
  console.info('SIGINT signal received')
  await stopHTTPServer()
  await closeDBConnection()
  process.exit(0)
})

// per i test
app.pid = process.pid
module.exports = {
  app,
  init: function (cb) {
    initCallback = cb
    return app
  }
}
