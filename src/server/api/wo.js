const express = require('express')
const router = express.Router()
const workbooks = require('../workbooks.json');

router.get('/', (req, res, next) => {
  res.status(200).json(workbooks)
})

router.get('/workbook/:code', (req, res, next) => {
  const workbook = workbooks.find(c => c.code === req.params.code)
  res.status(200).json(workbook)
})

module.exports = router
