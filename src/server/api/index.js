const express = require('express')
const router = express.Router()
const isAuth = require('../middlewares').isAuth

router.use('/auth', require('./auth'))
router.use('/to', require('./to'))
router.use('/wo', require('./wo'))

router.use('/calcoli', require('./user/calcoli'))
router.use('/enti', require('./user/enti'))
router.use('/ordini', require('./user/ordini'))
router.use('/prodotti', require('./user/prodotti'))
router.use('/help', require('./user/help'))
router.use('/click', require('./user/click'))

router.use('/admin/acquisti', require('./admin/acquisti'))
router.use('/admin/calcoli', require('./admin/calcoli'))
router.use('/admin/enti', require('./admin/enti'))
router.use('/admin/ordini', require('./admin/ordini'))
router.use('/admin/richieste', require('./admin/richieste'))
router.use('/admin/utenti', require('./admin/utenti'))

router.use('/crui/ordini', require('./crui/ordini'))

const exportPDF = require('../lib/exportPDF')
const exportOffertaCrui = require('../lib/exportOffertaCrui')
const exportOrdineCrui = require('../lib/exportOrdineCrui')
router.post('/exportpdf/exportOrdine/', isAuth, exportPDF.exportOrdine)
router.post('/exportpdf/exportOffertaCrui/', isAuth, exportOffertaCrui.exportOrdine)
router.post('/exportpdf/exportOrdineCrui/', isAuth, exportOrdineCrui.exportOrdine)

const Cos = require('../lib/cloud_object_storage')

// Cloud Object Storage
router.post('/cos/upload/:id', isAuth, Cos.upload)
router.post('/cos/postupload/:id', isAuth, Cos.postUpload)
router.get('/cos/get/', isAuth, Cos.getItem)
router.delete('/cos/delete', isAuth, Cos.deleteItem)

// const Zoom = require('../lib/zoom')

// router.get('/zoom/get', isAuth, Zoom.getMeeting)

module.exports = router
