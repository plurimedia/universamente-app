const express = require('express')
const router = express.Router()
const isAdmin = require('../../middlewares').isAdmin
const sql = require('../../db')
const email = require('../../lib/email')

router.get('/utenti', isAdmin, async (req, res) => {
  const users = await sql`
    select u.*, e.nome ateneo
      from enti e right outer join utenti u on (e.ente_id = u.ente_id)
     order by u.mail
  `
  res.status(200).json(users)
})

router.post('/aggiorna_utente', isAdmin, async (req, res) => {
  let host = `${req.protocol}://${req.headers.host}`
  const [mail] = await sql`
    update utenti set ${ sql(req.body, 'ente_id', 'abilitato', 'fl_ordini') }
     where utente_id = ${ req.body.utente_id }
    returning mail
  `
  if (req.body.notifyUser) {
    email.utenteAbilitato(mail.mail, host, res)
  } else {
    res.status(200).json({esito: 'ok'})
  }
})

module.exports = router
