const express = require('express')
const router = express.Router()
const isAdmin = require('../../middlewares').isAdmin
const sql = require('../../db')

router.get('/elenco', isAdmin, async (req, res) => {
  const enti = await sql`
    select e.ente_id, e.nome, e.fte,
           (select count(*)
              from enti
             where padre_id = e.ente_id) ndip,
           (select sum(a.fte)
              from acquisti a
             where a.ente_id = e.ente_id
               and a.scadenza > now()) totfte
      from enti e
     where e.padre_id is null
       and e.codice is not null
     order by e.nome
  `
  res.status(200).json(enti)
})

router.get('/storico', isAdmin, async (req, res) => {
  const arr = await sql`
    select e.ente_id, e.nome ente, e.fte fte_ente, a.acquisto_id, a.scadenza,
           a.fte, a.utente_id, a.mail, a.prodotto_id, a.prodotto, a.descrizione
      from enti e
      join v$acquisti a on a.ente_id = e.ente_id and a.scadenza <= now()
     where e.ente_id  = ${ req.query.id }
        or e.padre_id = ${ req.query.id }
     order by e.nome, a.scadenza nulls last
  `
  res.status(200).json(arr)
})

router.post('/save', isAdmin, async (req, res) => {
  let new_row

  if (req.body.ente_id)
    [new_row] = await sql`
      update enti set ${
        sql(req.body, 'nome', 'fte', 'codice')
      } where ente_id = ${ req.body.ente_id }
    `
  else
    [new_row] = await sql`
      insert into enti ${sql(req.body, 'nome', 'fte', 'padre_id')} returning *
    `

  res.status(200).json(new_row)
})

router.delete('/:id', isAdmin, async (req, res) => {
  await sql`
    delete from enti where ente_id = ${ req.params.id }
  `
  res.status(200).json({ esito: 'ok' })
})

module.exports = router
