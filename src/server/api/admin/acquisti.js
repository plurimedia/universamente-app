const express = require('express')
const router = express.Router()
const isAdmin = require('../../middlewares').isAdmin
const sql = require('../../db')

router.post('/save', isAdmin, async (req, res) => {
  let new_row
  req.body.utente_id = req.user.utente_id

  if (req.body.acquisto_id)
    [new_row] = await sql`
      update acquisti set ${
        sql(req.body, 'prodotto_id', 'fte', 'scadenza')
      } where acquisto_id = ${ req.body.acquisto_id }
    `
  else
    [new_row] = await sql`
      insert into acquisti ${sql(req.body, 'prodotto_id', 'fte', 'scadenza', 'ente_id', 'utente_id')} returning *
    `

  res.status(200).json(new_row)
})

router.delete('/:id', isAdmin, async (req, res) => {
  await sql`
    delete from acquisti where acquisto_id = ${ req.params.id }
  `
  res.status(200).json({ esito: 'ok' })
})

module.exports = router
