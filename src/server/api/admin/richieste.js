const express = require('express')
const router = express.Router()
const isAdmin = require('../../middlewares').isAdmin
const sql = require('../../db')

router.get('/list', isAdmin, async (req, res) => {
  const data = await sql`
    select c.*, e.nome ateneo, u.mail
      from richieste c, enti e, utenti u
     where c.utente_id = u.utente_id
       and e.ente_id   = u.ente_id
     order by data_richiesta desc
  `
  res.status(200).json(data)
})

router.post('/aggiorna_stato', isAdmin, async (req, res) => {
  await sql`
    update richieste
       set is_new = ${ req.body.is_new }
     where richiesta_id = ${ req.body.richiesta_id }
  `
  res.status(200).json({esito: 'ok'})
})

router.get('/:id/prodotti', isAdmin, async (req, res) => {
  const data = await sql`
    select c.prodotto_id, u.nome, u.descrizione
      from richieste_prodotti c, prodotti u
     where c.richiesta_id = ${ req.params.id }
       and c.prodotto_id  = u.prodotto_id
     order by u.ordinamento
  `
  res.status(200).json(data)
})

module.exports = router
