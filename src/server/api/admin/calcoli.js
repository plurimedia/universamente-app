const express = require('express')
const router = express.Router()
const isAdmin = require('../../middlewares').isAdmin
const sql = require('../../db')

router.get('/fte', isAdmin, async (req, res) => {
  const calcs = await sql`
    select c.*, e.nome ateneo, u.mail
      from calcoli_fte c, enti e, utenti u
     where c.utente_id = u.utente_id
       and e.ente_id   = u.ente_id
     order by data_calcolo desc
  `
  res.status(200).json(calcs)
})

module.exports = router
