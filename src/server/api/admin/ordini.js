const express = require('express')
const router = express.Router()
const isAdmin = require('../../middlewares').isAdmin
const sql = require('../../db')

router.get('/list', isAdmin, async (req, res) => {
  const arr = await sql`
    select ente, utente, ordine_id, data_ordine, stato, ateneo, codice, data_evasione
      from v$ordini
     order by data_ordine desc
  `
  res.status(200).json(arr)
})

router.get('/utenti', isAdmin, async (req, res) => {
  const arr = await sql`
    select u.utente_id, u.ente_id, u.admin, '[' || e.nome || '] - ' || u.nome nome, u.mail, e.fte
      from utenti u, enti e
     where u.fl_ordini
       and u.abilitato
       and u.admin   = 0
       and u.ente_id = e.ente_id
     order by e.nome, u.nome
  `
  res.status(200).json(arr)
})

router.post('/aggiorna_ordine', isAdmin, async (req, res) => {
  await sql.begin(async sql => {
    req.body.forEach(element => {
      sql`
        update ordini_enti
           set account_rh = ${ element.account_rh },
               login_id   = ${ element.login_id },
               rinnovo    = ${ element.rinnovo }
         where ordine_id = ${ element.ordine_id }
           and ente_id   = ${ element.ente_id }
      `
    })
  })

  res.status(200).json({esito: 'ok'})
})

router.post('/aggiorna_stato', isAdmin, async (req, res) => {
  await sql`
    update ordini
       set stato = ${ req.body.stato }, data_evasione = now()
     where ordine_id = ${ req.body.ordine_id }
  `
  res.status(200).json({esito: 'ok'})
})

router.post('/save', isAdmin, async (req, res) => {
  let ordine
  let pattern = /(\d{2})\/(\d{2})\/(\d{4})/
  let host = `${req.protocol}://${req.headers.host}`
  req.body.utente_id = req.body.utente.utente_id
  req.body.ente_id = req.body.utente.ente_id

  await sql.begin(async sql => {
    [ordine] = await sql`
      insert into ordini ${ sql(req.body, 'utente_id', 'ente_id', 'citta', 'indirizzo', 'cap', 'provincia', 'telefono', 'fax', 'cf') }
      returning *
    `

    req.body.prodotti.forEach(element => {
      element.ordine_id = ordine.ordine_id
      if (element.decorrenza) // da dd/mm/yyyy a yyyy-mm-dd
        element.decorrenza = element.decorrenza.replace(pattern, '$3-$2-$1')
      else
        element.decorrenza = null
    })
    await sql`
      insert into ordini_prodotti ${ sql(req.body.prodotti) }
    `

    req.body.enti.forEach(element => {
      element.ordine_id = ordine.ordine_id
    })
    await sql`
      insert into ordini_enti ${ sql(req.body.enti) }
    `
    const newDate = Date.parse(ordine.data_ordine)
    const date = new Date(newDate)

    const codiceData = ('0' + date.getDate()).slice(-2) + ('0' + (date.getMonth() + 1)).slice(-2) + date.getFullYear()
    let codice = 'OA' + leadingZeroes(ordine.ordine_id, 4) + codiceData + 'CR'

    function leadingZeroes (number, length) {
      let myString = '' + number
      while (myString.length < length) myString = '0' + myString
      return myString
    }

    await sql`
      update ordini
         set codice = ${ codice}
       where ordine_id = ${ ordine.ordine_id }
    `

    res.status(200).send(ordine)
  })
})

module.exports = router
