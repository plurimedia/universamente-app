const express = require('express')
const router = express.Router()
const corsiTO = require('../technicalOverview.json');

router.get('/', (req, res, next) => {
  res.status(200).json(corsiTO)
})

router.get('/corso/:code', (req, res, next) => {
  const corso = corsiTO.find(c => c.code === req.params.code)
  res.status(200).json(corso)
})

module.exports = router
