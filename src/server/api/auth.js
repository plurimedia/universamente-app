const express = require('express')
const router = express.Router()
const crypto = require('crypto')
const passport = require('passport')
const isAuth = require('../middlewares').isAuth
const isAdmin = require('../middlewares').isAdmin
const isCrui = require('../middlewares').isCrui
const auth = require('../lib/auth')
const email = require('../lib/email')
const sql = require('../db')

router.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) {
      res.status(401).json({ success: false, message: 'Autenticazione fallita' })
      return next(err)
    }

    if (!user) {
      res.status(401).json({ success: false, message: 'Autenticazione fallita' })
      return next('Autenticazione fallita')
    }

    req.login(user, (err) => {
      if (err) return next(err)
      return res.send(user)
    })
  })(req, res, next)
})

router.post('/registra', async (req, res, next) => {
  let host = `${req.protocol}://${req.headers.host}`
  let pwd = req.body.password
  const hash = crypto.createHash('sha512')
  hash.update(pwd)
  pwd = hash.digest('hex') // converto password in sha512-hex per scriverla sul db

  const utente = {
    mail: req.body.username,
    nome: req.body.nome,
    password: pwd,
    ente_id: req.body.ateneo,
    fl_ordini: true
  }

  try {
    const [new_row] = await sql`insert into utenti ${sql(utente)} returning *`
    email.nuovoUtente(host, res)
  } catch (err) {
    console.log('Err /registra', err)
    res.status(500).json({ success: false, message: 'Registrazione fallita ' + err.detail })
  }
})

router.get('/logout', (req, res, next) => {
  req.logout((err) => {
    if (err) return next(err)
    res.status(200).json()
  })
})

router.get('/is-authenticated', isAuth, async (req, res) => {
  res.status(200).json(req.session.passport)
})

router.get('/is-admin', isAdmin, async (req, res) => {
  res.status(200).json(req.session.passport)
})

router.get('/is-crui', isCrui, async (req, res) => {
  res.status(200).json(req.session.passport)
})

router.get('/current-user', isAuth, async (req, res) => {
  res.status(200).json(req.user)
})

router.post('/forgot-password', async (req, res, next) => {
  let host = `${req.protocol}://${req.headers.host}`
  try {
    await auth.resetPassword(req.body.email, host, res)
  } catch (err) {
    return next(err)
  }
})

router.get('/validate-token/:token', async (req, res, next) => {
  if (req.params && req.params.token)
  {
    try {
      await auth.validateToken(req.params.token)
      res.status(200).send()
    } catch (err) {
      return next(err)
    }
  }
})

router.post('/reset-password', async (req, res, next) => {
  try {
    await auth.changePassword(req.body)
    res.status(200).send()
  } catch (err) {
    return next(err)
  }
})

module.exports = router
