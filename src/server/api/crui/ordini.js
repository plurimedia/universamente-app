const express = require('express')
const router = express.Router()
const isCrui = require('../../middlewares').isCrui
const sql = require('../../db')

router.get('/list', isCrui, async (req, res) => {
  const arr = await sql`
    select ente, utente, ordine_id, data_ordine, stato, ateneo, codice
      from v$ordini
     where stato is not null
       and stato in ('F', 'C', 'O')
     order by data_ordine desc
  `
  res.status(200).json(arr)
})

router.post('/aggiorna_ordine', isCrui, async (req, res) => {
  await sql`
    update ordini
       set stato = 'C'
     where ordine_id = ${ req.body.ordine_id }
       and stato = 'F'
  `
  res.status(200).json({esito: 'ok'})
})

module.exports = router
