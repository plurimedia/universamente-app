const express = require('express')
const router = express.Router()
const sql = require('../../db')
const isAuth = require('../../middlewares').isAuth
const email = require('../../lib/email')

router.get('/list', isAuth, async (req, res) => {
  let ordini = []
  if (req.user.fl_ordini)
    ordini = await sql`
      select ordine_id, data_ordine, stato, ente, codice
        from v$ordini
       where utente_id = ${ req.user.utente_id }
       order by data_ordine desc
    `
  res.status(200).json(ordini)
})

router.post('/aggiorna_note', isAuth, async (req, res) => {
  if (req.user.fl_ordini)
    await sql`
      update ordini
         set note = ${ req.body.note }
       where ordine_id = ${ req.body.ordine_id }
    `
  res.status(200).json({esito: 'ok'})
})

router.get('/:id', isAuth, async (req, res) => {
  let ordine = null
  if (req.user.fl_ordini)
    [ordine] = await sql`
      select *
        from v$ordini
       where ordine_id = ${ req.params.id }
    `
  if (!ordine) {
    res.status(404).send({ esito: 'Ordine non trovato' })
    return
  }

  ordine.prodotti = await sql`
    select periodo, prodotto, descrizione, qta, show_qta, codice, decorrenza, classe,
           show_periodo, val_unitario::float, val_unitario_3::float, sconto::float, tu
      from v$ordini_prodotti
     where ordine_id = ${ req.params.id }
     order by prodotto
  `

  ordine.documenti = await sql`
    select nome, cos_id, data_upload, utente
      from v$ordini_documenti
     where ordine_id = ${ req.params.id }
     order by data_upload
  `
  ordine.enti = await sql`
    select *
      from v$ordini_enti
     where ordine_id = ${ req.params.id }
  `
  res.status(200).json(ordine)
})

router.post('/save', isAuth, async (req, res) => {
  let ordine
  let pattern = /(\d{2})\/(\d{2})\/(\d{4})/
  let host = `${req.protocol}://${req.headers.host}`
  req.body.utente_id = req.user.utente_id
  
  if (!req.user.fl_ordini) {
    res.status(200).json({esito: 'ok'})
    return
  }

  await sql.begin(async sql => {
    [ordine] = await sql`
      insert into ordini ${ sql(req.body, 'utente_id', 'ente_id', 'citta', 'indirizzo', 'cap', 'provincia', 'telefono', 'fax', 'cf') }
      returning *
    `

    req.body.prodotti.forEach(element => {
      element.ordine_id = ordine.ordine_id
      if (element.decorrenza) // da dd/mm/yyyy a yyyy-mm-dd
        element.decorrenza = element.decorrenza.replace(pattern, '$3-$2-$1')
      else
        element.decorrenza = null
    })
    await sql`
      insert into ordini_prodotti ${ sql(req.body.prodotti) }
    `

    req.body.enti.forEach(element => {
      element.ordine_id = ordine.ordine_id
    })
    await sql`
      insert into ordini_enti ${ sql(req.body.enti) }
    `
    const newDate = Date.parse(ordine.data_ordine)
    const date = new Date(newDate)

    const codiceData = ('0' + date.getDate()).slice(-2) + ('0' + (date.getMonth() + 1)).slice(-2) + date.getFullYear()
    let codice = 'OA' + leadingZeroes(ordine.ordine_id, 4) + codiceData + 'CR'

    function leadingZeroes (number, length) {
      let myString = '' + number
      while (myString.length < length) myString = '0' + myString
      return myString
    }

    await sql`
      update ordini
         set codice = ${ codice}
       where ordine_id = ${ ordine.ordine_id }
    `
  })

  email.offertaInserita(host, res)
})

module.exports = router
