const express = require('express')
const router = express.Router()
const isAuth = require('../../middlewares').isAuth
const sql = require('../../db')

router.post('/salva', isAuth, async (req, res) => {
  req.body.utente_id = req.user.utente_id
  const [new_row] = await sql`
    insert into calcoli_fte ${sql(req.body)} returning *
  `
  res.status(200).json(new_row)
})

module.exports = router
