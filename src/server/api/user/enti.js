const express = require('express')
const router = express.Router()
const sql = require('../../db')
const isAuth = require('../../middlewares').isAuth

router.get('/atenei', async (req, res) => {
  const enti = await sql`
    select ente_id, nome, codice
      from enti
     where padre_id is null
       and codice is not null
     order by nome
  `
  res.status(200).json(enti)
})

router.get('/:id', isAuth, async (req, res) => {
  const arr = await sql`
    select e.ente_id, e.nome ente, e.fte fte_ente, a.acquisto_id, a.scadenza,
           a.fte, a.utente_id, a.mail, a.prodotto_id, a.prodotto, a.descrizione
      from enti e
      left join v$acquisti a on a.ente_id = e.ente_id and a.scadenza > now()
     where e.ente_id  = ${ req.params.id }
        or e.padre_id = ${ req.params.id }
     order by e.nome, a.scadenza nulls last
  `
  res.status(200).json(arr)
})

router.get('/fte/:id', isAuth, async (req, res) => {
  const arr = await sql`
    select fte
      from enti
    where ente_id  = ${ req.params.id }
  `
  res.status(200).json(arr)
})

router.get('/:id/dipartimenti', isAuth, async (req, res) => {
  const arr = await sql`
    select ente_id, nome, fte
      from enti e
     where padre_id = ${ req.params.id }
  `
  res.status(200).json(arr)
})

module.exports = router
