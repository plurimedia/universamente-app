const express = require('express')
const router = express.Router()
const sql = require('../../db')
const isAuth = require('../../middlewares').isAuth
const isAdmin = require('../../middlewares').isAdmin

router.get('/', isAdmin, async (req, res) => {
  const clicks = await sql`
    select c.data_click, c.code, c.url, u.mail
      from clicks c, utenti u
     where c.utente_id = u.utente_id
     order by data_click
  `
  res.status(200).json(clicks)
})

router.post('/', isAuth, async (req, res) => {
  req.body.utente_id = req.user.utente_id
  await sql`
    insert into clicks ${ sql(req.body) }
  `
  res.status(200).json({esito: 'ok'})
})

module.exports = router
