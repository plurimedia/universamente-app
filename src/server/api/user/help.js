const express = require('express')
const router = express.Router()
const isAuth = require('../../middlewares').isAuth
const axios = require('axios')

const sourceId = process.env.CHATPDF_SOURCE_ID

router.post('/', isAuth, async (req, res) => {
    const config = {
        headers: {
          'x-api-key': process.env.CHATPDF_X_API_KEY,
          'Content-Type': 'application/json',
        },
      }
  
      const data = {
        sourceId: sourceId, // Use the sourceId provided
        referenceSources: true,
        messages: [
          {
            role: 'user',
            content: req.body.currentQuestion,
          },
        ],
      }

      await axios.post('https://api.chatpdf.com/v1/chats/message', data, config)
      .then(response => {
        res.status(200).json(response.data)
      })
      .catch(error => {
        console.error('Error:', error.message)
        res.status(500).send('Errore nella richiesta')
      })         
})

module.exports = router