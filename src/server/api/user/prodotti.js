const express = require('express')
const router = express.Router()
const sql = require('../../db')
const email = require('../../lib/email')

router.get('/prodotti', async (req, res) => {
  const prodotti = await sql`
    select *
      from prodotti
     order by ordinamento
  `
  res.status(200).json(prodotti)
})

router.post('/save_richiesta', async (req, res) => {
  let host = `${req.protocol}://${req.headers.host}`
  req.body.utente_id = req.user.utente_id

  await sql.begin(async sql => {
    const [richiesta] = await sql`
      insert into richieste ${ sql(req.body, 'utente_id', 'telefono', 'ente', 'testo' ) }
      returning *
    `

    let prodotti = []
    req.body.prodotti.forEach(element => {
      prodotti.push({ prodotto_id: element, richiesta_id: richiesta.richiesta_id })
    })

    if (prodotti.length)
      await sql`
        insert into richieste_prodotti ${ sql(prodotti) }
        returning *
      `
    return [richiesta]
  })

  email.richiestaInfo(host, res)
})

router.post

module.exports = router
