const postgres = require('postgres')

let opts = {
  transform: {
    undefined: null
  },
  idle_timeout: 20,
  max_lifetime: 60 * 30
}

if (process.env.NODE_ENV === 'development') opts.debug = console.log

const sql = postgres(opts)

module.exports = sql
