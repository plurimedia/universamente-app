const routes = [
  {
    path: '/',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('pages/Home.vue'),
      },
      {
        path: '/registrazione',
        name: 'registrazione',
        component: () => import('pages/Registrazione.vue'),
      },
      {
        path: '/cambio-password',
        name: 'cambiopassword',
        component: () => import('pages/ResetPassword.vue'),
      },
      {
        path: '/modifica-password/:token',
        name: 'modificapassword',
        component: () => import('pages/ModificaPassword.vue'),
        props: true
      },
      {
        path: '/',
        redirect: 'home'
      }
    ]
  },
  {
    path: '/user',
    component: () => import('layouts/UserLayout.vue'),
    children: [
      {
        path: '/user/dashboard',
        name: 'dashboard',
        component: () => import('pages/user/Dashboard.vue'),
      },
      {
        path: '/user/calcola',
        name: 'calcola',
        component: () => import('pages/user/Calcola.vue'),
      },
      {
        path: '/user/info',
        name: 'info',
        component: () => import('pages/user/Info.vue'),
      },
      {
        path: '/user/ordiniUtente',
        name: 'ordiniUtente',
        component: () => import('pages/user/OrdiniUtente.vue'),
      },
      {
        path: '/user/help',
        name: 'help',
        component: () => import('pages/HelpPage.vue')
      },
      {
        path: '/user/technical-overview',
        name: 'technicalOverview',
        component: () => import('pages/user/TechnicalOverview.vue')
      },
      {
        path: '/user/technical-overview/:code',
        name: 'course',
        component: () => import('pages/user/TechnicalOverviewCourse.vue')
      },
      {
        path: '/user/workbooks',
        name: 'workbooks',
        component: () => import('pages/user/WorkbooksList.vue')
      },
      {
        path: '/user/workbook/:code',
        name: 'workbook',
        component: () => import('pages/user/WorkbookDetail.vue')
      },
      {
        path: '/',
        redirect: 'dashboard'
      }
    ]
  },
  {
    path: '/admin',
    component: () => import('layouts/AdminLayout.vue'),
    children: [
      {
        path: '/admin/enti',
        name: 'enti',
        component: () => import('pages/admin/Elenco.vue')
      },
      {
        path: '/admin/enti/:id',
        name: 'ente',
        component: () => import('pages/admin/Ente.vue'),
        props: true
      },
      {
        path: '/admin/FTEcalcolati',
        name: 'ftecalcolati',
        component: () => import('pages/admin/FTEcalcolati.vue')
      },
      {
        path: '/admin/requests',
        name: 'requests',
        component: () => import('pages/admin/Richieste.vue')
      },
      {
        path: '/admin/ordini',
        name: 'ordini',
        component: () => import('pages/admin/Ordini.vue')
      },
      {
        path: '/admin/users',
        name: 'users',
        component: () => import('pages/admin/Utenti.vue')
      },
      {
        path: '/',
        redirect: 'enti'
      }
    ]
  },
  {
    path: '/crui',
    component: () => import('layouts/CruiLayout.vue'),
    children: [
      {
        path: '/crui/ordini',
        name: 'ordiniCrui',
        component: () => import('pages/crui/OrdiniCrui.vue')
      },
      {
        path: '/',
        redirect: 'ordiniCrui'
      }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error_404.vue')
  }
]

export default routes
