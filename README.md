# Plurimedia Universamente (universamente)

Plurimedia Universamente

## Install the dependencies
```bash
yarn
```

## Set environment variables
```bash
.env NOT included: configure manually
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
yarn dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Quasar CLI problems
```bash
quasar info
```
If the output says "@quasar/cli - undefined"

1) check node version node -v)
2) run the following (use a real CLI, NOT a code editor)

```bash
npm -g cache clean --force
npm -g cache verify
npm cache clean --force
npm cache verify
npm -g uninstall @quasar/cli
npm -g install @quasar/cli
npm uninstall @quasar/cli
rm -fr node_modules
rm package-lock.json
npm i
<now close and reopen the terminal app>
quasar info
```


### Postgres utilities

-- Tables with many rows (> 1000/5000) and low % (< 90) probably need to be indexed
SELECT relname,
   CASE idx_scan
     WHEN 0 THEN 'Insufficient data'
     ELSE (100 * idx_scan / (seq_scan + idx_scan))::text
   END percent_of_times_index_used,
   n_live_tup rows_in_table
 FROM
   pg_stat_user_tables
 ORDER BY
   n_live_tup DESC;


-- For a typical web application making a lot of short requests, target > 99% here.
SELECT
  'index hit rate' AS name,
  (sum(idx_blks_hit)) / nullif(sum(idx_blks_hit + idx_blks_read),0) AS ratio
FROM pg_statio_user_indexes
UNION ALL
SELECT
 'table hit rate' AS name,
  sum(heap_blks_hit) / nullif(sum(heap_blks_hit) + sum(heap_blks_read),0) AS ratio
FROM pg_statio_user_tables;

-- Ideally empty, otherwise indexes listed are probably useless
SELECT
  schemaname || '.' || relname AS table,
  indexrelname AS index,
  pg_size_pretty(pg_relation_size(i.indexrelid)) AS index_size,
  idx_scan as index_scans
FROM pg_stat_user_indexes ui
JOIN pg_index i ON ui.indexrelid = i.indexrelid
WHERE NOT indisunique AND idx_scan < 50 AND pg_relation_size(relid) > 5 * 8192
ORDER BY pg_relation_size(i.indexrelid) / nullif(idx_scan, 0) DESC NULLS FIRST,
pg_relation_size(i.indexrelid) DESC;

SELECT
  (total_time / 1000 / 60) as total_minutes,
  (total_time/calls) as average_time,
  query
FROM pg_stat_statements
ORDER BY 1 DESC
LIMIT 50;

SELECT * FROM pg_available_extensions;

CREATE extension pg_stat_statements;

### Auth0
```bash
npm i @auth0/auth0-spa-js passport-auth0 querystring --save

.env:
AUTH0_DOMAIN=
AUTH0_CLIENT_ID=
AUTH0_REDIRECT_URI=
AUTH0_CLIENT_SECRET=

Client:
src/auth/index.js
src/boot/auth.js
src/boot/route-guard.js
src/layouts/* (logout button)
quasar.conf.js (in build: env: require('dotenv').config().parsed)

Server:
src/server/api/auth.js
src/server/index.js (passport.use(new Auth0Strategy(...))
src/server/middlewares.js
```
Azure: https://www.mckennaconsultants.com/integrating-auth0-with-azure-active-directory/
